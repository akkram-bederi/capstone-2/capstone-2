const Item = require (`./../models/items`)
const CryptoJS = require("crypto-js");
const jwt = require("jsonwebtoken");
const {createToken}=require(`./../auth`)

//CREATE A MENU ITEM

module.exports.createItem=async(reqBody) => {
	//return await .save()
	const {itemName,description,category,price,isAvailable} =reqBody

	const newItem = new Item ({
		itemName:itemName,
		description:description,
        category:category,
		price:price,
		isAvailable:isAvailable
	})

	return await newItem.save().then(result =>{
		if(result){
			return true
		} else {
			if(result == null){
				return false
			}
		}
	})
}


// GET ALL MENU ITEMS
module.exports.getAllItems = async () => {
    return await Item.find().then(result => result)
}


// RETRIEVE MENU ITEM INFO
module.exports.getItem = async (id) => {
    return await Item.findById(id).then((result, err) => {
        if(result){
            return result
        } else{
            if(result == null){
                return {message: `Item does not exist`}
            } else {
                return err
            }
        }
    })
}

// UPDATE MENU ITEM 
module.exports.updateItem = async (id, reqBody) => {
    return await Item.findByIdAndUpdate(id, {$set: reqBody}, {new: true}).then((result, err) => {
        console.log(result)
        if(result) {
            return result
        } else {
            return err
        }
    })
}

// ARCHIVE MENU ITEM
module.exports.itemArchive = async (courseId) => {
	console.log(`hi`)
	return await Item.findOneAndUpdate(courseId, {$set: {isAvailable: false}}, {new:true}).then((result, err) => result ? true : err)
}

// UNARCHIVE MENU ITEM
module.exports.itemUnarchive = async (courseId) => {
	return await Item.findOneAndUpdate(courseId, {$set: {isAvailable: true}}, {new:true}).then((result, err) => result ? true : err)
}

//GET ALL ACTIVE MENU ITEMS
module.exports.availableItems = async () => {
    return await Item.find({isAvailable: true}).then((result, err) => {
        if(result){
            return result
        } else {
            if(result == null){
                return `No available items`
            } else {
                return err
            }
        }
    })
}

//GET ALL MEALS
module.exports.getMeals = async () => {
    return await Item.find({category: "Meal"}).then((result, err) => {
        console.log(result)
        if(result){
            return result
        } else {
            if(result == null){
                return `No meals`
            } else {
                return err
            }
        }
    })
}
//GET ALL BURGERS
module.exports.getBurgers = async () => {
    return await Item.find({category: "Burger"}).then((result, err) => {
        if(result){
            return result
        } else {
            if(result == null){
                return `No burgers`
            } else {
                return err
            }
        }
    })
}
//GET ALL SIDES
module.exports.getSides = async () => {
    return await Item.find({category: "Side"}).then((result, err) => {
        console.log(result)
        if(result){
            return result
        } else {
            if(result == null){
                return `No sides`
            } else {
                return err
            }
        }
    })
}
//GET ALL BEVERAGES
module.exports.getBeverages = async () => {
    return await Item.find({category: "Beverage"}).then((result, err) => {
        if(result){
            return result
        } else {
            if(result == null){
                return `no beverages`
            } else {
                return err
            }
        }
    })
}
//GET ALL ITEMS ON DISCOUNT
module.exports.getOnSale = async () => {
    return await Item.find({sale:{onSale:true}}).then((result, err) => {
        if(result){
            return result
        } else {
            if(result == null){
                return `No items on sale`
            } else {
                return err
            }
        }
    })
}