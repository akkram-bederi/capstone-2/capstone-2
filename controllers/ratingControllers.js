const Rating = require (`./../models/ratings`)
const CryptoJS = require("crypto-js");
const jwt = require("jsonwebtoken");
const {createToken}=require(`./../auth`)

//CREATE A RATING

module.exports.createRating=async(userId, reqBody) => {
	//return await .save()
	const {itemId,itemName,rating,review}=reqBody



	const newRating = new Rating ({
		userId:userId,
		itemId:itemId,
		itemName:itemName,
		rating:rating,
		review:review
	})

	return await newRating.save().then(result =>{
		if(result){
			return true
		} else {
			if(result == null){
				return false
			}
		}
	})
}


// GET ALL RATINGS
module.exports.getAllRatings = async () => {
    return await Rating.find().then(result => result)
}


// GET RATING
module.exports.getRating = async (id) => {
    return await Rating.findById(id).then((result, err) => {
        if(result){
            return result
        } else{
            if(result == null){
                return {message: `Rating does not exist`}
            } else {
                return err
            }
        }
    })
}

// UPDATE RATING
module.exports.updateRating = async (id, reqBody) => {
    return await Rating.findByIdAndUpdate(id, {$set: reqBody}, {new: true}).then((result, err) => {
        console.log(result)
        if(result) {
            return result
        } else {
            return err
        }
    })
}
