const Order = require (`./../models/orders`)
const CryptoJS = require("crypto-js");
const jwt = require("jsonwebtoken");
const {createToken}=require(`./../auth`)

//CREATE A ORDER

module.exports.createOrder=async(reqBody) => {
	//return await .save()
	const {itemName,quantity,price,totalAmount}=reqBody

	const newOrder = new Order ({
		itemName:itemName,
		quantity:quantity,
		price:price,
		totalAmount:totalAmount
	})

	return await newOrder.save().then(result =>{
		if(result){
			return true
		} else {
			if(result == null){
				return false
			}
		}
	})
}


// GET ALL ORDERS
module.exports.getAllOrders = async () => {
    return await Order.find().then(result => result)
}

//GET ALL COMPLETED ORDERS
module.exports.getAllcompOrders=async()=>{
    return await Order.find({status:"completed"}).then(result =>{
        console.log(result)
        if(result){
            return result
        }else{
            return `No completed Orders yet`
        }
    })
}

////GET ALL COMPLETED ORDERS
module.exports.getAllpendOrders=async()=>{
    return await Order.find({status:"Pending"}).then(result =>{
        console.log(result)
        if(result){
            return result
        }else{
            return `No pending orders`
        }
    })
}


// GET ORDER
module.exports.getOrder = async (id) => {
    return await Order.findById(id).then((result, err) => {
        if(result){
            return result
        } else{
            if(result == null){
                return {message: `Order does not exist`}
            } else {
                return err
            }
        }
    })
}

// UPDATE ORDER
module.exports.updateOrder = async (id, reqBody) => {
    return await Order.findByIdAndUpdate(id, {$set: reqBody}, {new: true}).then((result, err) => {
        console.log(result)
        if(result) {
            return result
        } else {
            return err
        }
    })
}

module.exports.statusComp = async(id,reqBody)=>{
    return await Order.findByIdAndUpdate(id, {$set:{status:"completed"}},{new:true}).then((result,err) =>{
        if(result){
            return result
        }else{
            return err
        }      
    })
}