const express=require(`express`);
const {
	register,
	getAllUsers,
	getAdmins,
	getUsers,
	checkEmail,
	login,
	profile,
	update,
	updatePw,
	updateAdminT,
	updateAdminF,
	deleteUser,
	createOrder
}=require(`./../controllers/userControllers`);

const router = express.Router()
const {verify,decode,verifyAdmin} =require(`./../auth`)
// const delete=require(`./../controllers/userControllers`)



//REGISTER A USER
router.post(`/register`, async (req,res)=>{
	console.log(req.body)
	try{
		await register(req.body).then(response => res.send(response))
	} catch(err){
		res.status(500).json(err)
	}
})

//GET ALL USERS
router.get('/', async (req, res) => {

	try{
		await getAllUsers().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

//GET ALL ADMINS
router.get('/admins', verifyAdmin, async (req, res) => {

	try{
		await getAdmins().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

//GET ALL USERS
router.get('/users', verifyAdmin, async (req, res) => {

	try{
		await getUsers().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

//CHECK IF EMAIL ALREADY EXISTS
router.post(`/email-exists`, async (req, res)=>{
	console.log(`hi`)
	try{
		await checkEmail(req.body.email).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//LOGIN USER
router.post(`/login`, async (req,res) => {
	try{
		await login(req.body).then(result => res.send(result))
	} catch(err) {
		res.status(500).json(err)
	}
})


//RETRIEVE USER INFO
router.get(`/:userId/`, verify, async(req,res)=>{
	const userId=decode(req.headers.authorization).id
	// console.log(userId)
	try{
		 profile(userId).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//UPDATE USER INFORMATION
router.put(`/update`, verify, async(req,res)=>{
	const userId=decode(req.headers.authorization).id
	console.log(userId)
	try{
		 await update(userId, req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//UPDATE USER PASSWORD
router.put(`/updatePw`, verify, async(req,res)=>{
	const userId=decode(req.headers.authorization).id
	//console.log(userId)
	try{
		 await updatePw(userId, req.body.password).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})


//ADMIN
// router.patch(`/isAdmin`, verify, async(req,res)=>{
// 	const admin=decode(req.headers.authorization).isAdmin
// 	try{
// 		if(admin==true){
// 			res.send(`You are authorized`)
// 		 	await updateAdmin(req.body).then(result => res.send(result))
// 		}else{
// 			res.send(`You are not authorized`)
// 		}
// 	}catch(err){
// 		res.status(500).json(err)
// 	}
// })

//ADMIN TO TRUE
router.patch(`/isAdminT`, verifyAdmin, async(req,res)=>{
	const admin=decode(req.headers.authorization).isAdmin
	try{
		await updateAdminT(req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//ADMIN TO FALSE
router.patch(`/isAdminF`, verifyAdmin, async(req,res)=>{
	try{
		await updateAdminF(req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})


//DELETE USER
router.delete(`/deleteUser`, verifyAdmin, async(req,res)=>{
	try{
		 deleteUser(req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//USER ADD TO CART
router.post('/addToCart', verify, async (req, res) => {
	const userId = decode(req.headers.authorization).id
	// const data ={
	// 	userId: decode(req.headers.authorization).id,
	// 	courseId: req.body.courseId
	// }
	try{
		await addToCart(userId,req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})	


// USER ADD ORDER
router.post('/order', verify, async (req, res) => {
	const userId = decode(req.headers.authorization).id
	// const data ={
	// 	userId: decode(req.headers.authorization).id,
	// 	courseId: req.body.courseId
	// }
	try{
		await createOrder(userId,req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})	



module.exports = router;

