const express = require(`express`);
const router = express.Router();

const {
    createRating,
    getAllRatings,
    getRating,
    updateRating,
} = require (`./../controllers/ratingControllers`)

const {verify, decode, verifyAdmin} = require(`./../auth`);

//CREATE A NEW RATING
router.post(`/createRating`, verifyAdmin, async(req,res)=>{
	const userId = decode(req.headers.authorization).id
	try{
		createRating(userId,req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})	

//GET ALL RATINGS

router.get(`/`, async(req,res)=>{
	try{
		getAllRatings().then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

// //GET RATING
router.get(`/:ratingId/`, async(req,res)=>{
	const id=req.params.ratingId
	console.log(req.params)
	console.log(id)
	try{
		getRating(id).then(result => res.send(result))
	}catch (err){
		res.status(500).json(err)
	}
})

//UPDATE RATING
router.patch(`/:ratingId/updateRating`, verifyAdmin, async(req,res)=>{
	const id=req.params.ratingId
	try{
		updateRating(id,req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

module.exports = router;
