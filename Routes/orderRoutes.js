const express = require(`express`);
const router = express.Router();

const {
    createOrder,
    getAllOrders,
    getAllcompOrders,
    getAllpendOrders,
    statusComp,
    getOrder,
    updateOrder,
} = require (`./../controllers/orderControllers`)

const {verifyCourse, decodeCourse, verifyAdmin} = require(`./../auth`);

//CREATE A NEW ORDER
router.post(`/createOrder`, verifyAdmin, async(req,res)=>{
	try{
		createOrder(req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})	

//GET ALL ORDERS

router.get(`/`, verifyAdmin, async(req,res)=>{
	try{
		getAllOrders().then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//GET ALL COMPLETED ORDERS
router.get(`/completedOrders`, verifyAdmin, async(req,res)=>{
	try{
		getAllcompOrders().then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}	
})

//GET ALL PENDING ORDERS
router.get(`/pendingOrders`, verifyAdmin, async(req,res)=>{
	try{
		getAllpendOrders().then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}	
})

// //GET ORDER
router.get(`/:orderId/`, async(req,res)=>{
	const id=req.params.itemId
	console.log(req.params)
	console.log(id)
	try{
		getOrder(id).then(result => res.send(result))
	}catch (err){
		res.status(500).json(err)
	}
})

//UPDATE ORDER
router.patch(`/:orderId/updateOrder`, verifyAdmin, async(req,res)=>{
	const id=req.params.orderId
	try{
		updateOrder(id,req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//UPDATE COMPLETED ORDER
router.patch(`/:orderId/statusComp`, verifyAdmin, async(req,res)=>{
	const id=req.params.orderId
	try{
		statusComp(id,req.body).then(result=>res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

module.exports = router;
