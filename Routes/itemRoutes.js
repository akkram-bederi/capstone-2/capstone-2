const express = require(`express`);
const router = express.Router();

const {
    createItem,
    getAllItems,
    getMeals,
    getBurgers,
    getSides,
    getBeverages,
    getOnSale,
    getItem,
    updateItem,
    itemArchive,
    itemUnarchive,
    availableItems,
} = require (`./../controllers/itemControllers`)

const {verifyCourse, decodeCourse, verifyAdmin} = require(`./../auth`);

//CREATE A NEW ITEM
router.post(`/createItem`, verifyAdmin, async(req,res)=>{
	try{
		createItem(req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})	

//GET ALL MENU ITEM

router.get(`/`, async(req,res)=>{
	try{
		getAllItems().then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//GET ALL AVAILABLE ITEMS
router.get(`/isAvailable`, async (req, res) => {
	try{
		await availableItems().then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//GET ALL MEAL ITEMS
router.get(`/meals`, async (req, res) => {
	try{
		await getMeals().then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//GET ALL BURGERS
router.get(`/burgers`, async (req, res) => {
	try{
		await getBurgers().then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//GET ALL SIDES
router.get(`/sides`, async (req, res) => {
	try{
		await getSides().then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//GET ALL BEVERAGES
router.get(`/beverages`, async (req, res) => {
	try{
		await getBeverages().then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//GET ALL ON SALE ITEMS
router.get(`/onSale`, async (req, res) => {
	try{
		await getOnSale().then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

// //GET SPECIFIC COURSE
router.get(`/:itemId/`, async(req,res)=>{
	const id=req.params.itemId
	console.log(req.params)
	console.log(id)
	try{
		getItem(id).then(result => res.send(result))
	}catch (err){
		res.status(500).json(err)
	}
})

//UPDATE A MENU ITEM
router.patch(`/:itemid/updateItem`, verifyAdmin, async(req,res)=>{
	const id=req.params.itemId
	try{
		updateItem(id,req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//SOLD OUT MENU ITEM
router.patch(`/:itemid/soldOut`, verifyAdmin, async(req,res)=>{
	const id=req.params.itemId
	try{
		await itemArchive(id).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//RETURN MENU ITEM
router.patch(`/:itemid/returnItem`, verifyAdmin, async(req,res)=>{
	const id=req.params.itemId
	try{
        await itemUnarchive(id).then(result => res.send(result))
    } catch(err){
        		res.status(500).json(err)
        	}
})





module.exports = router;
