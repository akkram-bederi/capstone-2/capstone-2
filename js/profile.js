let params=new URLSearchParams(document.location.search)
const id = params.get("userId")
const adminCheck= localStorage.getItem(`isAdmin`)
const token = localStorage.getItem(`token`)

const users=document.getElementById(`users`)
const products=document.getElementById(`products`)
const addProductForm=document.getElementById(`addProductForm`)



const userLength=document.getElementById(`userLength`)
const adminLength=document.getElementById(`adminLength`)
const mealLength=document.getElementById(`mealLength`)
const burgerLength=document.getElementById(`burgerLength`)
const sideLength=document.getElementById(`sideLength`)
const beverageLength=document.getElementById(`beverageLength`)


const tableAdmins=document.getElementById(`tableAdmins`)
const tableUsers=document.getElementById(`tableUsers`)
const tableMeals=document.getElementById(`tableMeals`)
const tableBurgers=document.getElementById(`tableBurgers`)
const tableSides=document.getElementById(`tableSides`)
const tableBeverages=document.getElementById(`tableBeverages`)


const ul=document.getElementById('ul')
const tableOrder=document.getElementById(`tableOrder`)
const logout=document.getElementById(`logout`)
const userId=document.getElementById('userId')
const fullName=document.getElementById('fullName')
const email=document.getElementById('email')
const contact=document.getElementById('contact')
const gender=document.getElementById('gender')
const bday=document.getElementById('bday')
const address=document.getElementById('address')

logout.addEventListener("click", (e)=>{


	localStorage.clear()
	window.location.replace(`../index.html`)
})

addProductForm.addEventListener(`submit`, (e)=>{
	e.preventDefault
	fetch(`http://localhost:3009/api/item/createItem`,{
		method:"POST",
		headers:{
			"Content-Type":"application/json",
			"Authorization":`Bearer ${token}`
		},
		body:JSON.stringify({
			itemName:document.getElementById(`itemName`).value,
			description:document.getElementById(`itemDesc`).value,
			category:document.getElementById(`itemCategory`).value,
			price:document.getElementById(`itemPrice`).value
		})
	})
	.then(result=>result.json())
	.then(result=>{
		alert(`Product successfully added`)
		window.location.replace(`./menu-template.html`)
	})
})

if(adminCheck==false){

}


if(adminCheck=="false"){


	ul.innerHTML=
		`
		<li><a href="#">Edit Profile</a></li>
		<li><a href="#myOrders">My Orders</a></li>
		<li><a href="#">My Addresses</a></li>
		<li><a href="#">Payment Methods</a></li>
		<li><a href="#">Settings</a></li>	
		`	

		users.style.display = "none";
		products.style.display = "none";


	

fetch(`http://localhost:3009/api/users/${id}`,{
	method:"GET",
	headers:{
		"Authorization":`Bearer ${token}`
	}
})
.then(result => result.json())
.then(result => {
	
	userId.innerHTML=`UserId: ${result._id}`
	fullName.innerHTML=`Full Name: ${result.firstName} ${result.lastName}`
	email.innerHTML=`Email: ${result.email}`
	contact.innerHTML=`Contact: ${result.contact}`
	gender.innerHTML=`Gender: ${result.gender}`
	bday.innerHTML=`Birthday: ${result.bday}`
	address.innerHTML=`Address: ${result.address}`

	const resultOrder = result.Order

	orders = resultOrder.map(orders =>{
		const{itemName,_id,totalAmount,status}=orders
		const orderNum = resultOrder.indexOf(orders)+1
		return(
			`
			<tr>
			  <th scope="row">${orderNum}</th>
			  <td>${_id}</td>
			  <td>${itemName}</td>
			  <td>${totalAmount}</td>
			  <td>${status}</td>
			</tr>

			`
		)	
	}).join("")
	tableOrder.innerHTML=orders
})

}else {
	ul.innerHTML=
		`
		<li><a href="#">Edit Profile</a></li>
		<li><a href="#myOrders">Manage Orders</a></li>
		<li><a href="#users">Manage Users</a></li>
		<li><a href="#products">Manage Products</a></li>
		<li><a href="#">Business Insights</a></li>
		<li><a href="#">Settings</a></li>
		`	


	fetch(`http://localhost:3009/api/users/${id}`,{
	method:"GET",
	headers:{
		"Authorization":`Bearer ${token}`
	}
})
.then(result => result.json())
.then(result => {

	userId.innerHTML=`UserId: ${result._id}`
	fullName.innerHTML=`Full Name: ${result.firstName} ${result.lastName}`
	email.innerHTML=`Email: ${result.email}`
	contact.innerHTML=`Contact: ${result.contact}`
	gender.innerHTML=`Gender: ${result.gender}`
	bday.innerHTML=`Birthday: ${result.bday}`
	address.innerHTML=`Address: ${result.address}`


	fetch(`http://localhost:3009/api/orders/`,{
		method:"GET",
		headers:{
			"Authorization":`Bearer ${token}`
		}
	})
	.then(result => result.json())
	.then(result => {
	

	orders = result.map(orders =>{
		const{itemName,_id,totalAmount,status}=orders
		const orderNum = result.indexOf(orders)+1
		return(
			`
			<tr>
			  <th scope="row">${orderNum}</th>
			  <td>${_id}</td>
			  <td>${itemName}</td>
			  <td>${totalAmount}</td>
			  <td>${status}</td>
			</tr>

			`
		)
	}).join("")
	tableOrder.innerHTML=orders
	})
})

fetch(`http://localhost:3009/api/users/users`,{
		method:"GET",
		headers:{
			"Authorization":`Bearer ${token}`
		}
	})
	.then(result => result.json())
	.then(result => {
	userLength.innerHTML=`${result.length}`

	usersRow = result.map(users =>{
		const {
			_id:userId,
			firstName:userFN,
			lastName:userLN,
			email:userEmail,
			createdAt:userDate
			}=users

			
		const userNum = result.indexOf(users)+1
		return(
			`
			<tr>
			  <th scope="row">${userNum}</th>
			  <td>${userId}</td>
			  <td>${userFN} ${userLN}</td>
			  <td>${userEmail}</td>
			  <td>${userDate}</td>
			  <td><button class="btn btn-danger" id="${userId}Delete"><small>Delete</small></button>
			  <button class="btn btn-info" id="${userId}Admin"><small>Make Admin</small></button></td>
			</tr>

			`
		)
	}).join("")
	tableUsers.innerHTML=usersRow

	result.forEach(result =>{
		document.getElementById(`${result._id}Delete`).addEventListener(`click`,(e)=>{
			e.preventDefault()
			const email = result.email
		    console.log(email)
			fetch(`http://localhost:3009/api/users/deleteUser`,{
				method:"DELETE",
				headers:{
					"Content-Type":"application/json",
					"Authorization":`Bearer ${token}`
				},
				body:JSON.stringify({
					email:result.email
				})
			})
				.then(result => result)
				.then(result=>{
					alert(`User deleted`)
					window.location.replace(`./profile.html`)
				})
			})
		})

	result.forEach(result =>{
		document.getElementById(`${result._id}Admin`).addEventListener(`click`,(e)=>{
			e.preventDefault()
			const email = result.email
			fetch(`http://localhost:3009/api/users/isAdminF`,{
				method:"PATCH",
				headers:{
					"Content-Type":"application/json",
					"Authorization":`Bearer ${token}`
				},
				body:JSON.stringify({
					email:result.email
				})
			})
				.then(result => result)
				.then(result=>{
					alert(`User is now an Admin`)
					window.location.replace(`./profile.html`)
				})
			})
		})



})

fetch(`http://localhost:3009/api/users/admins`,{
		method:"GET",
		headers:{
			"Authorization":`Bearer ${token}`
		}
	})
	.then(result => result.json())
	.then(result => {
	
	adminLength.innerHTML=`${result.length}`

	adminRow = result.map(admin =>{


		const{
			firstName:adminFN, 
			lastName:adminLN, 
			_id:adminId, 
			email:adminEmail, 
			createdAt:adminDate}=admin
		const adminNum = result.indexOf(admin)+1
		return(
			`
			<tr>
			  <th scope="row">${adminNum}</th>
			  <td>${adminId}</td>
			  <td>${adminFN} ${adminLN}</td>
			  <td>${adminEmail}</td>
			  <td>${adminDate}</td>
			  <td><button class="btn btn-danger" id="${adminId}Delete"><small>Delete</small></button>
			  <button class="btn btn-info" id="${adminId}User"><small>Make User</small></button></td>
			</tr>

			`
		)
	}).join("")
	tableAdmins.innerHTML=adminRow

	result.forEach(result =>{
		document.getElementById(`${result._id}Delete`).addEventListener(`click`,(e)=>{
			e.preventDefault()
			const email = result.email
		    console.log(email)
			fetch(`http://localhost:3009/api/users/deleteUser`,{
				method:"DELETE",
				headers:{
					"Content-Type":"application/json",
					"Authorization":`Bearer ${token}`
				},
				body:JSON.stringify({
					email:result.email
				})
			})
				.then(result => result)
				.then(result=>{
					alert(`User deleted`)
					window.location.replace(`./profile.html`)
				})
			})
		})

	result.forEach(result =>{
		document.getElementById(`${result._id}User`).addEventListener(`click`,(e)=>{
			e.preventDefault()
			const email = result.email
			fetch(`http://localhost:3009/api/users/isAdminF`,{
				method:"PATCH",
				headers:{
					"Content-Type":"application/json",
					"Authorization":`Bearer ${token}`
				},
				body:JSON.stringify({
					email:result.email
				})
			})
				.then(result => result)
				.then(result=>{
					alert(`User is not an admin anymore`)
					window.location.replace(`./profile.html`)
				})
			})
		})
	})


fetch(`http://localhost:3009/api/item/meals`,{
		method:"GET",
		headers:{
			"Authorization":`Bearer ${token}`
		}
	})
	.then(result => result.json())
	.then(result => {
	
	mealLength.innerHTML=`${result.length}`

	mealRow = result.map(meal =>{


		const{_id, itemName, price, createdAt}=meal
		const mealNum = result.indexOf(meal)+1
		return(
			`
			<tr>
			  <th scope="row">${mealNum}</th>
			  <td>${_id}</td>
			  <td>${itemName}</td>
			  <td>${price}</td>
			  <td>${createdAt}</td>

			</tr>

			`
		)
	}).join("")
	tableMeals.innerHTML=mealRow
})

fetch(`http://localhost:3009/api/item/burgers`,{
		method:"GET",
		headers:{
			"Authorization":`Bearer ${token}`
		}
	})
	.then(result => result.json())
	.then(result => {

	burgerLength.innerHTML=`${result.length}`

	burgerRow = result.map(burger =>{


		const{_id, itemName, price, createdAt}=burger
		const burgerNum = result.indexOf(burger)+1
		return(
			`
			<tr>
			  <th scope="row">${burgerNum}</th>
			  <td>${_id}</td>
			  <td>${itemName}</td>
			  <td>${price}</td>
			  <td>${createdAt}</td>
			</tr>

			`
		)
	}).join("")
	tableBurgers.innerHTML=burgerRow
})

fetch(`http://localhost:3009/api/item/sides`,{
		method:"GET",
		headers:{
			"Authorization":`Bearer ${token}`
		}
	})
	.then(result => result.json())
	.then(result => {
	
	sideLength.innerHTML=`${result.length}`

	sideRow = result.map(sides =>{


		const{_id, itemName, price, createdAt}=sides
		const sidesNum = result.indexOf(sides)+1
		return(
			`
			<tr>
			  <th scope="row">${sidesNum}</th>
			  <td>${_id}</td>
			  <td>${itemName}</td>
			  <td>${price}</td>
			  <td>${createdAt}</td>
			</tr>

			`
		)
	}).join("")
	tableSides.innerHTML=sideRow
})

fetch(`http://localhost:3009/api/item/beverages`,{
		method:"GET",
		headers:{
			"Authorization":`Bearer ${token}`
		}
	})
	.then(result => result.json())
	.then(result => {
	
	beverageLength.innerHTML=`${result.length}`

	beverageRow = result.map(beverage =>{


		const{_id, itemName, price, createdAt}=beverage
		const beverageNum = result.indexOf(beverage)+1
		return(
			`
			<tr>
			  <th scope="row">${beverageNum}</th>
			  <td>${_id}</td>
			  <td>${itemName}</td>
			  <td>${price}</td>
			  <td>${createdAt}</td>
			</tr>

			`
		)
	}).join("")
	tableBeverages.innerHTML=beverageRow
})	
}



