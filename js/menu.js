const token = localStorage.getItem(`token`)
const admin = localStorage.getItem(`isAdmin`)
const id=localStorage.getItem(`id`)


const registerSubmit = document.getElementById('registerSubmit');
const loginSubmit=document.getElementById(`loginSubmit`)

const mealRow = document.getElementById(`meals`)
const burgerRow = document.getElementById(`burgers`)
const sideRow = document.getElementById(`sides`)
const beverageRow = document.getElementById(`beverages`)
const discountedRow = document.getElementById(`discounted`)
const buttons= document.getElementById(`buttons`)
const profile= document.getElementById(`profile`)
let meals, burgers, sides, beverages, discounted;
let cardFooter;



if(token != null && token != undefined){
	profile.innerHTML=
	`
	<a href='./profile.html?userId=${id}'>PROFILE</a>
	<button type="button" class="btn btn-outline-warning" id="logout">Logout</button>
	`

	const logout=document.getElementById(`logout`)
	logout.addEventListener("click", (e)=>{


	localStorage.clear()
	window.location.replace(`../index.html`)
	})

}else{
	profile.innerHTML=
	`
	<button type="button" class="btn btn-outline-warning" data-toggle="modal" data-target="#login" id="logout">Login</button>

	`
}


// if(admin==false){


																//MEALS


fetch(`http://localhost:3009/api/item/meals`,{
	method:"GET"
})
.then(result=>result.json())
.then(result =>{
	if(result.length<1){
		return `No meals`
	}else {
		meals = result.map(meals => {
			const {itemName, description, price, _id} = meals

			return(
				`
				<div class="col-6 col-sm-3">
					<a href="#" data-toggle="modal" data-target="#menu${_id}">
					<img src="../images/sampleMeal.png" width="200px" height="200px" class="img-fluid">
					</a>
					<p>${itemName}</p>
				</div>

				<div class="modal fade" id="menu${_id}" tabindex="-1" aria-labelledby="${_id}ModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered">
				    <div class="modal-content">
				      <div class="modal-header">
				      	<img src="../images/sampleMeal.png" width="300px" height="300px" class="img-fluid modal-title mx-auto">
				        
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				           	<span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <form id="${_id}Form">
				      <div class="modal-body text-left p-3 m-3">
				        <h4 id="${_id}OrderName"><strong>${itemName}</strong></h4>
				        <p>&#8369;<span id="${_id}OrderPrice">${price}</span></p>
				        <p>${description}</p>
				        <label for="quantity">Quantity:</label>
				        <input type="number" id="${_id}Quantity" required>
				      </div>
				      <div class="modal-footer">
				        <button type="submit" class="btn btn-warning btn-block" id="${_id}Order">Order</button>
				      </div>
				      </form>
				    </div>
				  </div>
				</div>

				`
			)

			}).join(" ")
			mealRow.innerHTML = meals
		} 

		result.forEach(result=>{
			const {_id:id}=result
			document.getElementById(`${id}Form`).addEventListener("submit",(e) =>{
				e.preventDefault()
				console.log(parseInt(document.getElementById(`${id}OrderPrice`).textContent))
				fetch(`http://localhost:3009/api/users/order`,{
					method:"POST",
					headers:{
						"Content-Type":"application/json",
						"Authorization":`Bearer ${token}`
					},
					body:JSON.stringify({
						itemId:id,
						itemName:document.getElementById(`${id}OrderName`).textContent,
						quantity:document.getElementById(`${id}Quantity`).value,
						price:document.getElementById(`${id}OrderPrice`).textContent,
						totalAmount:((parseInt(document.getElementById(`${id}OrderPrice`).textContent))*(parseInt(document.getElementById(`${id}Quantity`).value)))
					})
				})
				.then(result=>result.json)
				.then(result => {
					alert(`Order success`)
				})
				
			})
		})
		
	}) 


																	//BURGE


fetch(`http://localhost:3009/api/item/burgers`,{
	method:"GET"
})
.then(result=>result.json())
.then(result =>{
	if(result.length<1){
		return `No burgers`
	}else {
		burgers = result.map(burgers => {
			const {itemName, description, price, _id} = burgers

			return(
				`
				<div class="col-6 col-sm-3">
					<a href="#" data-toggle="modal" data-target="#menu${_id}">
					<img src="../images/sampleBurger.png" width="200px" height="200px" class="img-fluid">
					</a>
					<p>${itemName}</p>
				</div>

				<div class="modal fade" id="menu${_id}" tabindex="-1" aria-labelledby="${_id}ModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered">
				    <div class="modal-content">
				      <div class="modal-header">
				        <img src="../images/sampleBurger.png" width="300px" height="300px" class="img-fluid modal-title mx-auto">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				           	<span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <form id="${_id}Form">
				      <div class="modal-body text-left p-3 m-3">
				        <h4 id="${_id}OrderName"><strong>${itemName}</strong></h4>
				        <p>&#8369;<span id="${_id}OrderPrice">${price}</span></p>
				        <p>${description}</p>
				        <label for="quantity">Quantity:</label>
				        <input type="number" id="${_id}Quantity" required>
				      </div>
				      <div class="modal-footer">
				        <button type="submit" class="btn btn-warning btn-block" id="${_id}Order">Order</button>
				      </div>
				      </form>
				    </div>
				  </div>
				</div>

				`
			)
			}).join(" ")


			burgerRow.innerHTML = burgers
		}
		result.forEach(result=>{
			const {_id:id}=result
			document.getElementById(`${id}Form`).addEventListener("submit",(e) =>{
				e.preventDefault()
				console.log(parseInt(document.getElementById(`${id}OrderPrice`).textContent))
				fetch(`http://localhost:3009/api/users/order`,{
					method:"POST",
					headers:{
						"Content-Type":"application/json",
						"Authorization":`Bearer ${token}`
					},
					body:JSON.stringify({
						itemId:id,
						itemName:document.getElementById(`${id}OrderName`).textContent,
						quantity:document.getElementById(`${id}Quantity`).value,
						price:document.getElementById(`${id}OrderPrice`).textContent,
						totalAmount:((parseInt(document.getElementById(`${id}OrderPrice`).textContent))*(parseInt(document.getElementById(`${id}Quantity`).value)))
					})
				})
				.then(result=>result.json)
				.then(result => {
					alert(`Order success`)
				})
				
			})
		})
	})
															
														//SIDES


fetch(`http://localhost:3009/api/item/sides`,{
	method:"GET"
})
.then(result=>result.json())
.then(result =>{
	if(result.length<1){
		return `No sides`
	}else {
		sides = result.map(sides => {
			const {itemName, description, price, _id} = sides

			return(
				`
				<div class="col-6 col-sm-3">
					<a href="#" data-toggle="modal" data-target="#menu${_id}">
					<img src="../images/sampleSide.png" width="200px" height="200px" class="img-fluid">
					</a>
					<p>${itemName}</p>
				</div>

				<div class="modal fade" id="menu${_id}" tabindex="-1" aria-labelledby="${_id}ModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered">
				    <div class="modal-content">
				      <div class="modal-header">
				        <img src="../images/sampleSide.png" width="300px" height="300px" class="img-fluid modal-title mx-auto">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				           	<span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <form id="${_id}Form">
				      <div class="modal-body text-left p-3 m-3">
				        <h4 id="${_id}OrderName"><strong>${itemName}</strong></h4>
				        <p>&#8369;<span id="${_id}OrderPrice">${price}</span></p>
				        <p>${description}</p>
				        <label for="quantity">Quantity:</label>
				        <input type="number" id="${_id}Quantity" required>
				      </div>
				      <div class="modal-footer">
				        <button type="submit" class="btn btn-warning btn-block" id="${_id}Order">Order</button>
				      </div>
				      </form>
				    </div>
				  </div>
				</div>

				`
			)
			}).join(" ")


			sideRow.innerHTML = sides
		}
		result.forEach(result=>{
			const {_id:id}=result
			document.getElementById(`${id}Form`).addEventListener("submit",(e) =>{
				e.preventDefault()
				console.log(parseInt(document.getElementById(`${id}OrderPrice`).textContent))
				fetch(`http://localhost:3009/api/users/order`,{
					method:"POST",
					headers:{
						"Content-Type":"application/json",
						"Authorization":`Bearer ${token}`
					},
					body:JSON.stringify({
						itemId:id,
						itemName:document.getElementById(`${id}OrderName`).textContent,
						quantity:document.getElementById(`${id}Quantity`).value,
						price:document.getElementById(`${id}OrderPrice`).textContent,
						totalAmount:((parseInt(document.getElementById(`${id}OrderPrice`).textContent))*(parseInt(document.getElementById(`${id}Quantity`).value)))
					})
				})
				.then(result=>result.json)
				.then(result => {
					alert(`Order success`)
				})
				
			})
		})
	})
												
												//BEVERAGES


fetch(`http://localhost:3009/api/item/beverages`,{
	method:"GET"
})
.then(result=>result.json())
.then(result =>{
	if(result.length<1){
		return `No items`
	}else {
		beverages = result.map(beverages => {
			const {itemName, description, price, _id} = beverages

			return(
				`
				<div class="col-6 col-sm-3">
					<a href="#" data-toggle="modal" data-target="#menu${_id}">
					<img src="../images/sampleBeverage.png" width="200px" height="200px" class="img-fluid">
					</a>
					<p>${itemName}</p>
				</div>

				<div class="modal fade" id="menu${_id}" tabindex="-1" aria-labelledby="${_id}ModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered">
				    <div class="modal-content">
				      <div class="modal-header">
				        <img src="../images/sampleBeverage.png" width="300px" height="300px" class="img-fluid modal-title mx-auto">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				           	<span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <form id="${_id}Form">
				      <div class="modal-body text-left p-3 m-3">
				        <h4 id="${_id}OrderName"><strong>${itemName}</strong></h4>
				        <p>&#8369;<span id="${_id}OrderPrice">${price}</span></p>
				        <p>${description}</p>
				        <label for="quantity">Quantity:</label>
				        <input type="number" id="${_id}Quantity" required>
				      </div>
				      <div class="modal-footer">
				        <button type="submit" class="btn btn-warning btn-block" id="${_id}Order">Order</button>
				      </div>
				      </form>
				    </div>
				  </div>
				</div>

				`
			)
			}).join(" ")


			beverageRow.innerHTML = beverages
		}
		result.forEach(result=>{
			const {_id:id}=result
			document.getElementById(`${id}Form`).addEventListener("submit",(e) =>{
				e.preventDefault()
				fetch(`http://localhost:3009/api/users/order`,{
					method:"POST",
					headers:{
						"Content-Type":"application/json",
						"Authorization":`Bearer ${token}`
					},
					body:JSON.stringify({
						itemId:id,
						itemName:document.getElementById(`${id}OrderName`).textContent,
						quantity:document.getElementById(`${id}Quantity`).value,
						price:document.getElementById(`${id}OrderPrice`).textContent,
						totalAmount:((parseInt(document.getElementById(`${id}OrderPrice`).textContent))*(parseInt(document.getElementById(`${id}Quantity`).value)))
					})
				})
				.then(result=>result.json)
				.then(result => {
					alert(`Order success`)
				})
			})
		})
	})


											//DISCOUNTED ITEMS

// fetch(`http://localhost:3009/api/item/onSale`,{
// 	method:"GET"
// })
// .then(result=>result.json())
// .then(result =>{
// 	if(result.length<1){
// 		return `<div class="col-12 text-center">
// 				<h1>NO ITEMS ON SALE</h1>
// 				</div
// 				`
// 	}else {
// 		discounted = result.map(discounted => {
// 			const {itemName, description, price, _id} = discounted

// 			return(
// 				`
// 				<div class="col-6 col-sm-3">
// 					<img src="../images/sampleDiscounted.png" width="200px" height="200px" class="img-fluid">
// 					<p>${itemName}</p>
// 				</div>

// 				`
// 			)
// 			}).join(" ")

// 			// console.log(courses)

// 			discountedRow.innerHTML = discounted
// 		}
// 	})

loginSubmit.addEventListener("click", (e) =>{
	console.log(`hi`)

	const email = document.getElementById('email').value;
	const pw = document.getElementById('password').value;

	fetch(`http://localhost:3009/api/users/login`, {
		method:"POST",
		headers:{
			"Content-Type":"application/json"
		},
		body: JSON.stringify({
			email:email,
			password:pw
		})
	})
	.then(result => result.json())
	.then(result => {
		localStorage.setItem(`token`, result.token)
		let token = localStorage.getItem(`token`)

		//request for user information & store admin and id inthe local storage
		fetch(`http://localhost:3009/api/users/profile`,{
			method:"GET",
			headers:{
				"Authorization":`Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result =>{



		if(result){	
			console.log(result)
			localStorage.setItem(`id`, result._id)
			localStorage.setItem(`isAdmin`, result.isAdmin)
			alert(`Login successfuly.`)

			window.location.replace(`./menu-template.html`)
		}else{
			alert(`Something went wrong. Please try again.`)
		}
		})
	})
})




registerSubmit.addEventListener("click", (e) => {


	const fn = document.getElementById('firstName').value;
	const ln = document.getElementById('lastName').value;
	const email = document.getElementById('emailtext').value;
	const pw = document.getElementById('pw').value;
	const cpw = document.getElementById('cpw').value;
	console.log(`test`)
	console.log(pw)
	console.log(cpw)

	if(pw == cpw){
		console.log(`hi`)

		fetch(`http://localhost:3009/api/users/email-exists`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: fn,
				lastName: ln,
				email: email,
				password: pw
			})
		})
		.then(result => result.json())
		.then(result => {
			if(result == false){
				
				fetch(`http://localhost:3009/api/users/register`, {
					method: "POST",
					headers:{
						"Content-Type": "application/json",
					},
					body: JSON.stringify({
						//user input
						firstName: fn,
						lastName: ln,
						email: email,
						password: pw
					})
				})
				.then(result => result.json())
				.then(result => {
					
					if(result){
						alert('User successfully registered!')
						window.location.replace(`./index.html`)
					} else {
						alert(`Please try again`)
					}
				})

			} else {
				alert(`User already exists`)
			}
		})

	}else{
		alert(`Password does not match`)
	}
})