const registerSubmit = document.getElementById('registerSubmit');
const loginSubmit=document.getElementById(`loginSubmit`)
const profile= document.getElementById(`profile`)
const id=localStorage.getItem(`id`)
const token=localStorage.getItem(`token`)
const buttons=document.getElementById(`buttons`)

if(token != null && token != undefined){
	profile.innerHTML=
	`
	<a href='./pages/profile.html?userId=${id}'>PROFILE</a>
	<button type="button" class="btn btn-outline-warning" id="logout">Logout</button>
	`

	buttons.innerHTML=
	`
	<button class="btn btn-outline-warning" data-toggle="modal" data-target="#login"><a href="./pages/menu-template.html">Order Now</a></button>
	`
	const logout=document.getElementById(`logout`)
	logout.addEventListener("click", (e)=>{


	localStorage.clear()
	window.location.replace(`./index.html`)
	})

}else{
	profile.innerHTML=
	`
	<button type="button" class="btn btn-outline-warning" data-toggle="modal" data-target="#login" id="logout">Login</button>

	`

	buttons.innerHTML=
	`
	<button class="btn btn-outline-warning" data-toggle="modal" data-target="#login">Log in</button>
	<button class="btn btn-outline-warning" data-toggle="modal" data-target="#registermodal">Register</button>
	`
}


loginSubmit.addEventListener("click", (e) =>{
	console.log(`hi`)

	const email = document.getElementById('email').value;
	const pw = document.getElementById('password').value;

	fetch(`http://localhost:3009/api/users/login`, {
		method:"POST",
		headers:{
			"Content-Type":"application/json"
		},
		body: JSON.stringify({
			email:email,
			password:pw
		})
	})
	.then(result => result.json())
	.then(result => {
		localStorage.setItem(`token`, result.token)
		let token = localStorage.getItem(`token`)

		//request for user information & store admin and id inthe local storage
		fetch(`http://localhost:3009/api/users/profile`,{
			method:"GET",
			headers:{
				"Authorization":`Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result =>{



		if(result){	
			console.log(result)
			localStorage.setItem(`id`, result._id)
			localStorage.setItem(`isAdmin`, result.isAdmin)
			alert(`Login successfuly.`)

			window.location.replace(`./index.html`)
		}else{
			alert(`Something went wrong. Please try again.`)
		}
		})
	})
})




registerSubmit.addEventListener("click", (e) => {


	const fn = document.getElementById('firstName').value;
	const ln = document.getElementById('lastName').value;
	const email = document.getElementById('emailtext').value;
	const pw = document.getElementById('pw').value;
	const cpw = document.getElementById('cpw').value;
	console.log(`test`)
	console.log(pw)
	console.log(cpw)

	if(pw == cpw){
		console.log(`hi`)

		fetch(`http://localhost:3009/api/users/email-exists`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: fn,
				lastName: ln,
				email: email,
				password: pw
			})
		})
		.then(result => result.json())
		.then(result => {
			if(result == false){
				
				fetch(`http://localhost:3009/api/users/register`, {
					method: "POST",
					headers:{
						"Content-Type": "application/json",
					},
					body: JSON.stringify({
						//user input
						firstName: fn,
						lastName: ln,
						email: email,
						password: pw
					})
				})
				.then(result => result.json())
				.then(result => {
					
					if(result){
						alert('User successfully registered!')
						window.location.replace(`./index.html`)
					} else {
						alert(`Please try again`)
					}
				})

			} else {
				alert(`User already exists`)
			}
		})

	}else{
		alert(`Password does not match`)
	}
})