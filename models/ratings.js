const mongoose = require(`mongoose`);

const ratingsSchema = new mongoose.Schema({
    
    userId:{
        type:String,
        required:[true,`UserId is required`]
    },
    itemId:{
        type:String,
        required:[true,`ProductId is required`]
    },
    itemName:{
        type:String,
        required:[true,`Item Name is required`]
    },
    rating:{
        type:Number,
        default:5
    },
    review:{
        type:String,
        default:``
    }
}, {timestamps: true})

module.exports = mongoose.model("Ratings", ratingsSchema);