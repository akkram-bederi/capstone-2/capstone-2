const mongoose = require(`mongoose`);

const orderSchema = new mongoose.Schema({
    
    itemId:{
        type:String,
        require:[true,`UserId is required`]
    },
    orderedBy:{
        type:String,
        require:[true]
    },
    itemName:{
        type:String,
        require:[true, `Product name is required`]
    },
    quantity:{
        type:Number,
        default:1
    },
    price:{
        type:Number,
        require:[true,`How much are the items`]
    },
    totalAmount:{
        type:Number,
        required:[true, `Total Amount is required`]
    },
    status:{
        type:String,
        default:`Pending`    
    }
}, {timestamps: true})

module.exports = mongoose.model("Order", orderSchema);