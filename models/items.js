const mongoose = require(`mongoose`);

const itemSchema = new mongoose.Schema({
    itemName: {
        type: String,
        required: [true, `Course name is required`],
        unique: true
    },
    description: {
        type: String,
        required: [true, `Course description is required`]
    },
    category:{
        type:String,
        required:[true,`Category is required`]
    },
    price: {
        type: Number,
        required: [true, `Price is required`]
    },
    isAvailable: {
        type: Boolean,
        default: true
    },
    sale:[{
        onSale:{
            type:Boolean,
            default:false
        },
        discountedPrice:{
            type:Number,
            default:0
        },
        discountedUntil:{
            type:Date,
            default:`Not discounted`
        }
    }]
}, {timestamps: true})

module.exports = mongoose.model("Item", itemSchema);