const mongoose = require(`mongoose`)

const user1Schema = new mongoose.Schema(
	{
	firstName:{
		type:String,
		required:[true,`First name is required`]
	},
	lastName:{
		type:String,
		required:[true,`First name is required`]
	},
	email:{
		type:String,
		required:[true,`email is required`],
		unique:true
	},
	password:{
		type:String,
		required:[true,`password is required`]
	},
	address:{
		type:String,
	},
	isAdmin:{
		type:Boolean,
		default:false
	},
	Order:[{
		itemId:{
			type:String,
		},
		itemName:{
			type:String,
			required:[true,`Total Amount is required`]
		},
		price:{
			type:Number,
			required:[true,`Total Amount is required`]
		},
		quantity:{
			type: Number,
			required:[true,`Please select ID quantity`]
		},
		totalAmount:{
			type:Number,
			required:[true,`Total Amount is required`]
		},
		status: {
			type:String,
			default:`Pending`
		},
		orderedOn:{
			type:Date,
			default: new Date()
		}
	}],
	},	{timestamps:true})


// //Model
module.exports=mongoose.model(`User`,user1Schema)